const express = require('express')
const mongoose = require('mongoose');
const routes = require('./routes')
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use('/auth',routes.auth);
app.use('/user',routes.userRouter);
app.use('/post',routes.postRouter);
app.use('/category',routes.categoryRouter);
mongoose.connect('mongodb+srv://dbMurat:23734658070@cluster0.nc3sy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  }
)
        .then(() => {
            console.log('server started');
            app.listen(5000);
        })
        .catch(err => console.log(err));



