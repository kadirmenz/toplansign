const mongoose = require('mongoose')

const PostSchema = new mongoose.Schema({
    title:{type:String, required:true},
    category:{type:mongoose.Schema.Types.ObjectId, ref:'Category'},
    description:{type:String, required:true},
    time:{type:Date, default: Date.now}
}) 

module.exports = mongoose.model('Post',PostSchema);

/* ID: 1   
    name:asdasd,
    desc:asdsad,
    categoryId: Art => 1, Spor => 2
*/