const CategorySchema = require("./CategorySchema");
const PostSchema = require("./PostSchema");
const UserSchema = require("./UserSchema") ;

module.exports = {
    CategorySchema, 
    PostSchema, 
    UserSchema
};


/* (async() => {
    const newModel = new CategorySchema({
        name:'category1'
    })
    await newModel.save();
    console.log('saved')
})() */