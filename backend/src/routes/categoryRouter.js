const {Router} = require('express')
const controllers = require( "../controllers");
const router = Router();

router.post('/create',controllers.categoryController.createCategory);
router.get('/findAll',controllers.categoryController.findAll);
router.get('/:id',controllers.categoryController.findOne);
router.put('/:id',controllers.categoryController.updateOne);
router.delete('/:id',controllers.categoryController.deleteOne);

module.exports = router;


