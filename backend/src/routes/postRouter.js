const {Router} = require('express')
const controllers = require('../controllers')
const router = Router() 

router.post('/create',controllers.postController.createPost)
router.get('/:id',controllers.postController.findOne)
router.put('/:id',controllers.postController.updateOne)
router.delete('/:id',controllers.postController.deleteOne)
router.get('/',controllers.postController.findAll)

module.exports = router;
