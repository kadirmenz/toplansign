const categoryRouter = require('./categoryRouter');
const postRouter = require('./postRouter');
const userRouter = require('./userRouter');
const auth = require('./auth')
module.exports = {
    categoryRouter,
    postRouter,
    userRouter,
    auth
}