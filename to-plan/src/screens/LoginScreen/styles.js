import {StyleSheet, Dimensions} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#EAECDD',
    alignItems: 'center',
    
  },
  name: {
    fontSize: 42,
    color:'black',
    marginTop: '15%',
    marginBottom:'10%'
  }, 
  emailText:{
    color: 'black',
    fontSize: 18,
    marginTop:'5%'
  },
  inputEmail: {
    color: 'grey',
    fontSize: 20,
    paddingStart: '3%',
    marginTop: '2%',
    borderRadius:5,
    borderWidth:1,
    borderColor:'black',
    backgroundColor: '#EAECDD',
    width:windowWidth/1.2,
    height:windowHeight/15,
  },
  button: {
    width: windowWidth/3,
    height:windowHeight/15,
    backgroundColor: '#EAECDD',
    justifyContent:"center",
    alignItems:"center",
    borderWidth:1,
    borderColor:'black',
    marginTop:"5%",
    borderRadius:8,
   
  },
  buttonText:{
    color:'black',
    fontSize: 18
  },
  signButton:{
    fontSize: 16,
    color:'grey',
    marginTop:'3%'
  }

});

export default styles;
