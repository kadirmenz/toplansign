import React, { Component, useState, useEffect } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { registerUser, loginUser } from "../../../redux/actions/authAction";
import { useDispatch, useSelector } from "react-redux";
import styles from "./styles";
import BottomMenu from "../../components/BottomMenu";
import authReducer from "../../../redux/reducers/authReducer";
import AsyncStorage from "@react-native-community/async-storage";

function LoginScreen({ navigation }) {
  const auth = useSelector((state) => state.auth);
  const [token,setToken]=useState(null);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  const form = (

    <View style={styles.container}>
      <Text style={styles.name}> TO-PLAN </Text>
      <Text style={styles.emailText}> Enter your email </Text>
      <TextInput
        style={styles.inputEmail}
        placeholder="example@email.com"
        autoFocus
        
        onChangeText={(e) => setEmail(e)}
      />
      <Text style={styles.emailText}> Enter your password</Text>
      <TextInput
        style={styles.inputEmail}
        placeholder="********"
        secureTextEntry={true}
        autoFocus
        
        onChangeText={(e) => setPassword(e)}
      />
      <TouchableOpacity style={styles.button} onPress={() => onSubmit()}>
        <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
        <Text style={styles.signButton}>Sign Up</Text>
      </TouchableOpacity>
    </View >
  );
  
 
  const onSubmit = () => {
    console.log("email"+email)
    dispatch(loginUser(email,password,navigation));

    /*  return false; */
  };

  return (
    
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{ flex: 1 }}>
      {form}
    </KeyboardAvoidingView>
  );


}

function SignupScreen({ navigation }) {
  const register = useSelector((state) => state.register);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setpassword2] = useState("");
  const dispatch = useDispatch();
  
  const onSubmit = () => {
    console.log(email)
    dispatch(registerUser(email,password,password2,navigation));

    /*  return false; */
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{ flex: 1 }}
    >
      <View style={styles.container}>
        <Text style={styles.name}> TO-PLAN </Text>
        <Text style={styles.emailText}> Enter your email </Text>
        <TextInput
          style={styles.inputEmail}
          placeholder="example@email.com"
          autoFocus
          
          onChangeText={(e) => setEmail(e)}
        />
        <Text style={styles.emailText}> Enter your password</Text>
        <TextInput
          style={styles.inputEmail}
          placeholder="********"
          secureTextEntry={true}
          autoFocus
          
          onChangeText={(e) => setPassword(e)}
        />
        <Text style={styles.emailText}> Enter your password again</Text>
        <TextInput
          style={styles.inputEmail}
          placeholder="********"
          secureTextEntry={true}
          autoFocus
          value={password2}
          onChange={(e) => setpassword2(e.target.value)}
        />
        <TouchableOpacity style={styles.button} onPress={() => onSubmit()}>
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Text style={styles.signButton}>Go Login</Text>
        </TouchableOpacity>
        <View>
          <Text>
            {register.done === true && password === password2 ? "" : ""}
          </Text>
        </View>
      </View>
    </KeyboardAvoidingView>
  );
}

function LoadingScreen({ navigation }) {
  const auth = useSelector(state => state.auth);

  const detectLogin = async () => {
    const token = await AsyncStorage.getItem('token')

    if (token!=null) {
      navigation.replace("Menu")
    } else {
      navigation.replace("Login")
    }
  }

  useEffect(() => {
    detectLogin()
  }, []);


  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <ActivityIndicator size="large" color="blue" />
    </View>
  )

}


const Stack = createStackNavigator();

export function Login() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName="Loading"
      >
        <Stack.Screen name="Loading" component={LoadingScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="SignUp" component={SignupScreen} />
        <Stack.Screen name="Menu" component={BottomMenu} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Login;
