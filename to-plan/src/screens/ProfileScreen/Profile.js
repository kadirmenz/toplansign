import React, { Component } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, TextInput } from 'react-native';
import styles from './styles'
import { Ionicons, Feather } from '@expo/vector-icons';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import AsyncStorage from '@react-native-community/async-storage';


export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  onPressLogout=async()=>{
    await AsyncStorage.removeItem('token');
    this.props.navigation.replace("Login")
    
  }
  renderInner = () =>(
    <View style={styles.panel}>
      <View style={{alignItems: 'center'}}> 
         <Text style={styles.panelTitle}>Upload Photo</Text>
         <Text style={styles.panelSubtitle}>Choose Your Profile Picture</Text>
       </View>
 
       <TouchableOpacity style={styles.panelButton}>
         <Text style={styles.panelButtonTitle}>Take Photo</Text>
       </TouchableOpacity>
       <TouchableOpacity style={styles.panelButton}>
         <Text style={styles.panelButtonTitle}>Choose From Library</Text>
       </TouchableOpacity>
       <TouchableOpacity 
       onPress = {() => this.bs.current.snapTo(1)}
       style={styles.panelButton}>
         <Text style={styles.panelButtonTitle}>Cancel</Text>
       </TouchableOpacity>
       
     </View>
     );
 
   renderHeader = () => (
     <View style={styles.header}>
       <View style={styles.panelHeader}>
          <View style={styles.panelHandle}></View>
       </View>
     </View>
   );
   bs= React.createRef();
   fall= new Animated.Value(1);

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <BottomSheet
        ref={this.bs}
        snapPoints={[330,0]}
        renderContent={this.renderInner}
        renderHeader={this.renderHeader}
        initialSnap={1}
        callbackNode={this.fall}
        enabledGestureInteraction={true}
        />
        <TouchableOpacity style={styles.editProfile}>
           <Ionicons name="ios-cog" size={24} color="black" />

        </TouchableOpacity>
        
        <TouchableOpacity 
          style={styles.profilePicture}
          onPress = {() => this.bs.current.snapTo(0)}
        >
            <Ionicons name="ios-camera" size={24} color="black" />
        </TouchableOpacity>
        <Text style={styles.name} >Name </Text>

        <View style={styles.containerActivities}>
          <Text style={styles.sportartText}>Your Sport&Arts</Text>
          <TextInput 
            style={styles.sportartInput}
            placeholder='First Activity'
            placeholderTextColor="black"
            paddingStart='3%'
          > </TextInput>
            <TextInput 
            style={styles.sportartInput}
            placeholder='Second Activity'
            placeholderTextColor="black"
            paddingStart='3%'
          > </TextInput>
            <TextInput 
            style={styles.sportartInput}
            placeholder='Third Activity'
            placeholderTextColor="black"
            paddingStart='3%'
          > </TextInput>
            <TextInput 
            style={styles.sportartInput}
            placeholder='Fourth Activity'
            placeholderTextColor="black"
            paddingStart='3%'
          > </TextInput>
            <TextInput 
            style={styles.sportartInput}
            placeholder='Fifth Activity'
            placeholderTextColor="black"
            paddingStart='3%'
          > </TextInput>

            <TouchableOpacity
            style={styles.saveButton}
            >
              <Text style={styles.sportartText}>Save</Text>
            </TouchableOpacity>
            <TouchableOpacity
            onPress = {() => this.onPressLogout()}
            style={styles.saveButton}
            >
              <Text style={styles.sportartText}>Logout</Text>
            </TouchableOpacity>

            
        </View>

      

      </SafeAreaView>
    );
  }
}
