import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import styles from './styles';

export default class CreateAct extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.backArrow}
          onPress={() => this.props.navigation.goBack()} 
        >
          <Ionicons name="ios-arrow-back" size={30} color="black" />
        </TouchableOpacity>
        <Text style={styles.title}>TO-PLAN</Text>
        <Text style={styles.secTitle}>Create Activity</Text>
        <View style={styles.categories}>
            <TouchableOpacity style={styles.categoryBox}>
                <Text style={styles.categoryText} >ART</Text>
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.categoryBox}>
                <Text style={styles.categoryText}>SPORT</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.categoryBox}>
                <Text style={styles.categoryText}>OTHER</Text>
            </TouchableOpacity>
        </View>

        <TouchableOpacity style={styles.locConteiner}>
          <Text style={styles.categoryText} >Location</Text>
        </TouchableOpacity>

        <TextInput style={styles.locConteiner}
          placeholder= "Enter Title"
          paddingStart='3%'
        >
        </TextInput>

        <TextInput style={styles.infoConteiner}
          placeholder= "Enter Description"
          paddingStart='3%'
        >
        </TextInput>

        <TextInput style={styles.timeConteiner}
          placeholder= "Time"
          paddingStart='3%'
        >
        </TextInput>
      
        <TouchableOpacity  style={styles.createButton}>
          <Text style={styles.categoryText}>Create</Text>
        </TouchableOpacity> 

      </View>
    );
  }
}
