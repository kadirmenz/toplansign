import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles'
import Create from '../CreateScreen/CreateAct';
import Find from '../FindScreen/FindAct';
import AsyncStorage from '@react-native-community/async-storage';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.name}> TO-PLAN </Text>
        
        <TouchableOpacity 
         style={styles.findButton}
         onPress={() => this.props.navigation.navigate(Find) }
         >
          <Text>Find Activity</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.createButton}
          onPress={() => this.props.navigation.navigate(Create) }
        >
          
           <Text>Create Activity</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
