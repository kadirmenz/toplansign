import {StyleSheet, Dimensions} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    width: windowWidth,
    height: windowHeight,
    backgroundColor: '#EAECDD',
    alignItems: 'center'
  },
  title:{
    marginTop:'10%',
      fontSize:40
  },
  secTitle:{
      marginLeft:'35%',
      fontSize:16,
      color:'grey'
  },
  infoContainer:{
    marginTop:'5%',
    height: windowHeight/3,
    width: windowWidth/1.1,
    backgroundColor:'#fff',
    borderRadius:20,
    flexDirection:'column',
  },
  text:{
    fontSize:14,
    color:'grey'
  },
  titleText:{
    fontWeight:'600',
    fontSize:24
  },
  nameAndButton:{
    flexDirection:'row',
    justifyContent: 'space-between',
    alignItems:'center',
    paddingHorizontal:'5%',
    position:'absolute',
    bottom:'3%'
  },
  button:{
    marginLeft:'30%',
    height: windowHeight/16,
    width: windowWidth/4,
    borderWidth:1,
    borderColor:'black',
    borderRadius:10,
    alignItems:'center',
    justifyContent:'center'
  },
  backArrow:{
    position:'absolute',
    top:'5%',
    left:'3%'
  },
  textContainer:{
    marginLeft:'5%',
    marginTop:'2%'
  } 

});

export default styles;