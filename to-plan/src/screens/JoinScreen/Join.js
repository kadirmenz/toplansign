import React, { Component } from 'react';
import { View, Text, TouchableOpacity,ScrollView } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import styles from '../JoinScreen/styles';
import {useSelector} from 'react-redux'

const Join = props => {

  const postId = props.route.params;

  const post = useSelector(state => state.posts.find(posts => posts._id == postId))

  console.log(post)

    return (
        <ScrollView >
          <View style={styles.container}>
            <TouchableOpacity style={styles.backArrow}>
            <Ionicons name="ios-arrow-back" size={30} color="black" />
            </TouchableOpacity>

            <Text style={styles.title}>TO-PLAN</Text>
            <Text style={styles.secTitle}>Join Activity</Text>

            <View style={styles.infoContainer}>
              <View style={styles.textContainer}>
                <Text style={styles.titleText}>{post.title}</Text>
                <Text style={styles.text}>{post.description}</Text>
              </View>
                
                <View style={styles.nameAndButton}>
                    <Text style={styles.text}>activity owner profile</Text>
                    
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.text}>add friend</Text>
                    </TouchableOpacity>
                </View>
              </View>
            </View>

      </ScrollView>
    );
    
}


export default Join;
