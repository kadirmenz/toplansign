import {StyleSheet, Dimensions} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#EAECDD',
    alignItems: 'center'
  },
  backArrow:{
    position:'absolute',
    top:'5%',
    left:'3%'
    
},
title:{
    marginTop:'10%',
    fontSize:40
},
secTitle:{
    marginLeft:'35%',
    fontSize:16,
    color:'grey'
},
search:{
    backgroundColor: '#fff',
    marginTop: '4%',
    paddingHorizontal:'3%',
    justifyContent:'center',
    height: windowHeight / 17,
    width: windowWidth / 1.2,
    borderRadius: 25,
},

});

export default styles;
