import React, { Component, useEffect } from 'react';
import { View, Text, TouchableOpacity, TextInput, FlatList } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import {useDispatch, useSelector} from 'react-redux'
import Cards from '../../components/Cards/Cards';
import styles from './styles'

import {fetchposts} from '../../../redux/actions/postAction';


const FindAct = props => {

    const dispatch = useDispatch();

    const {activities} = useSelector(state => state.posts)

    useEffect(() =>{
      dispatch(fetchposts());
    }, [dispatch]);

 /*     const postList = (
      posts.activities.map(item => <View key={item._İd}>{item.title}</View>)
    ) 
 */
    return (
      <View style={styles.container}>
   {/*       <TouchableOpacity style={styles.backArrow}
          onPress={() => props.navigation.goBack()} 
        >
          <Ionicons name="ios-arrow-back" size={30} color="black" />
        </TouchableOpacity>
        <Text style={styles.title}>TO-PLAN</Text>
        <Text style={styles.secTitle}>Find Activity</Text>
        
        <TextInput
            style={styles.search}
            placeholder='Search Activity'   
        /> 
 */}
         <FlatList
          data={activities}
          keyExtractor={item => item._id}
          renderItem={({item}) => (    
          <Cards
              navigation={props.navigation}
     
          />)
          }
        /> 
{/*        
       
            <FlatList
          data={posts}
          keyExtractor={item => item._id}
          renderItem={({item}) => 
            <Cards
              navigation={props.navigation}
              title={item.title}
              category={item.category}
              description={item.description}
              time={item.time}
              id={item._id}
              item = {item}
          /> */}
          
      </View> 
    );
  
  } 

  export default FindAct;
