import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../screens/HomeScreen/Home';
import Find from '../screens/FindScreen/FindAct';
import Create from '../screens/CreateScreen/CreateAct';
import Join from '../screens/JoinScreen/Join';
import Friends from '../screens/FriendsScreen/Friends';
import Profile from '../screens/ProfileScreen/Profile';
import Menu from '../components/Router';
import Onboarding from '../screens/OnboardScreen/OnboardingScreen';
import { LoginScreen } from '../screens/LoginScreen/Login'

const AppNavigator = createStackNavigator();

const optionHeandler = () => ({
  headerShown: false,
});

export default class Navigation extends Component {

  render() {
    return (
      <NavigationContainer>
        <AppNavigator.Navigator initialRouteName="Menu">
          <AppNavigator.Screen
            name="Home"
            component={Home}
            options={optionHeandler}
          />
          <AppNavigator.Screen
            name="Find"
            component={Find}
            options={optionHeandler}
          />
          <AppNavigator.Screen
            name="Menu"
            component={Menu}
            options={optionHeandler}
          />
          <AppNavigator.Screen
            name="Create"
            component={Create}
            options={optionHeandler}
          />
          <AppNavigator.Screen
            name="Join"
            component={Join}
            options={optionHeandler}
          />
          <AppNavigator.Screen
            name="Friends"
            component={Friends}
            options={optionHeandler}
          />
          <AppNavigator.Screen
            name="Profile"
            component={Profile}
            options={optionHeandler}
          />
          <AppNavigator.Screen
            name="Onboarding"
            component={Onboarding}
            options={optionHeandler}
          />
          <AppNavigator.Screen
            name="LoginScreen"
            component={LoginScreen}
            options={optionHeandler}
          />
        </AppNavigator.Navigator>
      </NavigationContainer>
    );
  }
}

