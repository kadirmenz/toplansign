import React from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import { Ionicons, Feather } from '@expo/vector-icons';
import {createStackNavigator} from '@react-navigation/stack';

import Home from '../screens/HomeScreen/Home';
import CreateAct from '../screens/CreateScreen/CreateAct';
import FindAct from '../screens/FindScreen/FindAct';
import Friends from '../screens/FriendsScreen/Friends';
import Profile from '../screens/ProfileScreen/Profile';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const optionHeandler = () => ({
  headerShown: false,
});

function stackAct() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={optionHeandler}
      />
      <Stack.Screen
        name="CreateAct"
        component={CreateAct}
        options={optionHeandler}
      />
       <Stack.Screen
        name="FindAct"
        component={FindAct}
        options={optionHeandler}
      />
    </Stack.Navigator>
  );
}

export default function BottomMenu() {
  return (
      <Tab.Navigator
        shifting="true"
        inactiveColor="black"
        activeColor="black"
        barStyle={{backgroundColor: '#EAECDD'}}>
        <Tab.Screen
          options={{
            tabBarColor: '#EAECDD',
            tabBarIcon: ({color}) => (
              <Ionicons name="ios-home" color={color} size={22} />
            ),
          }}
          name="Home"
          component={stackAct}
          
        />
        <Tab.Screen
          options={{
            tabBarColor: '#EAECDD',
            tabBarIcon: ({color}) => (
              <Feather name="message-circle" color={color} size={22} />
            ),
          }}
          name="Friends"
          component={Friends}
        />

        <Tab.Screen
          options={{
            tabBarColor: '#EAECDD',
            tabBarIcon: ({color}) => (
              <Ionicons name="md-person" color={color} size={22} />
            ),
          }}
          name="Profile"
          
          component={Profile}
        />
      </Tab.Navigator>
   
  );
}

