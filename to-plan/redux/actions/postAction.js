export const FETCH_POST = 'FETCH_POST';

import axios from "axios"

export const fetchposts = () => {
    return async dispatch => {
        const res = await axios.get('https://localhost:5000/post');
        dispatch({
            type: FETCH_POST,
            payload: res.data
        })

    }
}