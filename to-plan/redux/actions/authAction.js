export const REGISTER_USER_PENDING = "REGISTER_USER_PENDING";
export const REGISTER_USER_SUCCESS = "REGISTER_USER_SUCCESS";
export const REGISTER_USER_FAIL = "REGISTER_USER_FAIL";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_FAIL = "LOGIN_USER_FAIL";
export const LOGIN_USER_PENDING = "LOGIN_USER_PENDING";

import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";

export const registerUser = (email, password, password2, navigation) => {
  /* const {email, password} = authData; */

  return async (dispatch) => {
    try {
      dispatch({
        type: "REGISTER_USER_PENDING",
      });
      //http://seninIpin:5000/auth/register
      fetch("http://192.168.1.33:5000/auth/register", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
          password2: password2,
        }),
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
            console.log(data)
          dispatch({
            type: "REGISTER_USER_SUCCESS",
            payload: data,
          });
          navigation.replace("Login");
        });
    } catch (error) {
      dispatch({
        type: "REGISTER_USER_FAIL",
        payload: error,
      });
    }
  };
};

export const loginUser = (email, password, navigation) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: LOGIN_USER_PENDING,
      });
      //http://seninIpin:5000/auth/login
      fetch("http://192.168.1.33:5000/auth/login", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      })
        .then((res) => {
          if (res.status == 201) {
            return res.json();
          }
        })
        .then(async (data) => {
          if (data != null) {
            const { token, user } = data;
            await AsyncStorage.setItem("token", token);
            dispatch({
              type: LOGIN_USER_SUCCESS,
              payload: data,
            });
            navigation.replace("Menu");
          } else {
            console.log(data);
          }
        });
    } catch (error) {
      dispatch({
        type: LOGIN_USER_FAIL,
        payload: error,
      });
    }
  };
};
