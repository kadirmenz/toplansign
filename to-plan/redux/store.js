import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'

import postReducer from './reducers/postReducer'
import authReducer from './reducers/authReducer';
import registerReducer from './reducers/registerReducer'

const rootReducer = combineReducers({
    posts: postReducer,
    auth: authReducer,
    register: registerReducer
});

const middleware = composeWithDevTools(applyMiddleware(thunk));

export default createStore(rootReducer,middleware);
