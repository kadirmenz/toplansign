import {FETCH_POST} from '../actions/postAction';

const initialState = {
    activities: [],
    //loading:false
}

export default function (state= initialState, action) {
    
    switch(action.type){
        case FETCH_POST:
            return{
                ...state,
                activities: action.payload,
                //loading:true
            }
        default:
            return state; 
    }

    
}