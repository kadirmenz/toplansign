import{
    REGISTER_USER_SUCCESS, 
    REGISTER_USER_PENDING,
    REGISTER_USER_FAIL,
} from '../actions/authAction';

const initialState = {
    user: {},
    pending: false,
    done:false,
    error: {}
}

export default function(state = initialState, action){

    switch(action.type){
        case REGISTER_USER_PENDING:
            return{
                ...state,
                pending:true
            }
        case REGISTER_USER_SUCCESS:
            return{
                ...state,
                user: action.payload, //{email : 'sdfsdf' , password : 'sdfsdf'} //{token : asdasdasdasd, user:{email:Passowr sdasdasd}}
                done:true
            }
            
        case REGISTER_USER_FAIL:
            return{
                ...state,
                error:action.payload
            }
        default:
            return state; 
                
    }
}